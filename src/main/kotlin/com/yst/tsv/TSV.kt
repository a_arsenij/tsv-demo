package com.yst.tsv

import kotlin.math.max
import kotlin.math.min

class Point(
        val x: Int,
        val y: Int
)

fun checkPointInTriangle(
        point: Point,
        a: Point, b: Point, c: Point
): Boolean {
    return true
}

fun drawTriangle(
        canvas: Array<Array<Color>>,
        a: Point,
        b: Point,
        c: Point
) {

    val minX = min(a.x, min(b.x, c.x))
    val maxX = max(a.x, max(b.x, c.x))
    val minY = min(a.y, min(b.y, c.y))
    val maxY = max(a.y, max(b.y, c.y))

    for (x in minX..maxX) {
        for (y in minY..maxY) {
            if (checkPointInTriangle(Point(x, y), a, b, c)) {
                canvas[x][y]=Color.black
            }
        }
    }
}


fun draw(
        width: Int,
        height: Int
): Array<Array<Color>> {

    val res = Array(width) { Array(height) { Color.white } }

    drawTriangle(
            res,
            Point(10, 10),
            Point(20, 10),
            Point(20, 20)
    )

    return res
}