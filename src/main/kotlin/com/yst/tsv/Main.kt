package com.yst.tsv

import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO


class Color(
        val red: Int,
        val green: Int,
        val blue: Int,
        val opacity: Int // 0 - opaque, 255 - transparent
) {
    companion object {
        val black = Color(0, 0, 0, 0)
        val red = Color(255, 0, 0, 0)
        val white = Color(255, 255, 255, 0)
    }
}

fun main(args: Array<String>) {
    val w = 800
    val h = 600
    val image = draw(w, h)
    val res = BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB)
    val g2 = res.createGraphics()
    for (x in 0 until w) {
        for (y in 0 until h) {
            val c = image[x][y]
            g2.color = java.awt.Color(c.red, c.green, c.blue, 255 - c.opacity)
            g2.fillRect(x, y, 1, 1)
        }
    }
    g2.dispose()
    ImageIO.write(res, "png", File("image.png"))
}